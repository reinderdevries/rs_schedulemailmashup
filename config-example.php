<?php
/**
 * config-example.php
 * @author Reinder de Vries <inbox@reinder.io>
 **/

// Database
define('RS_DB_HOST', '');
define('RS_DB_NAME', '');
define('RS_DB_USER', '');
define('RS_DB_PASS', '');

// Behaviour
define('confirmAfterScheduling', true);
define('confirmAfterSending', true);

// Other
define('originalInbox', 'inbox@reinder.io');




?>